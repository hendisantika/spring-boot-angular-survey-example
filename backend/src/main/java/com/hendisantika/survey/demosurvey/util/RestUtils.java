package com.hendisantika.survey.demosurvey.util;

import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/18
 * Time: 09.01
 * To change this template use File | Settings | File Templates.
 */
public final class RestUtils {

    private RestUtils() {
    }

    /**
     * Convert a comma delimited list into a list of {@code Long} values.
     *
     * @param value comma separated text
     * @return list of parsed elements
     */
    public static List<Long> commaDelimitedListToLongList(String value) {
        return StringUtils.commaDelimitedListToSet(value)
                .stream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
    }
}