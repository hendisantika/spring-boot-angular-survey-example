package com.hendisantika.survey.demosurvey.repository;

import com.hendisantika.survey.demosurvey.domain.UserResponse;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/18
 * Time: 22.30
 * To change this template use File | Settings | File Templates.
 */
public interface UserResponseDao {
    /**
     * Add a new responses of the user
     *
     * @param userResponses entities to save
     * @return an array of the number of rows affected by each statement
     * @throws DaoException             if there is an sql exception
     * @throws IllegalArgumentException if any given argument is invalid
     */
    int[] addUserResponses(List<UserResponse> userResponses);

    /**
     * Update responses of the user
     *
     * @param userResponses entities to save
     * @return an array of the number of rows affected by each statement
     * @throws DaoException             if there is an sql exception
     * @throws IllegalArgumentException if any given argument is invalid
     */
    int[] updateUserResponses(List<UserResponse> userResponses);

    /**
     * Find the responses for the provided questions and user
     *
     * @param userId       user who responded
     * @param questionsIds questions that the user may responded
     * @return list of responses
     * @throws DaoException             if there is an sql exception
     * @throws IllegalArgumentException if any given argument is invalid
     */
    List<UserResponse> findResponsesOfUserByUserIdAndQuestionIds(Long userId, List<Long> questionsIds);
}
