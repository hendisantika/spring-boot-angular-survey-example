package com.hendisantika.survey.demosurvey.controller;

import com.codahale.metrics.annotation.Timed;
import com.hendisantika.survey.demosurvey.dto.RestErrorDto;
import com.hendisantika.survey.demosurvey.dto.UserResponseForQuestionDto;
import com.hendisantika.survey.demosurvey.dto.UserResponsesForQuestionListDto;
import com.hendisantika.survey.demosurvey.service.UserResponseService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static com.hendisantika.survey.demosurvey.util.RestUtils.commaDelimitedListToLongList;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/18
 * Time: 09.02
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/api/v1/questions")
public class QuestionResponseController {

    /* because this example is not using Spring Security yet
     * the user id is simply hardcoded */
    private static final Long USER_ID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(QuestionResponseController.class);

    private final UserResponseService userResponseService;

    public QuestionResponseController(UserResponseService userResponseService) {
        this.userResponseService = userResponseService;
    }

    /**
     * GET  /:questionsId/responses/me : Get the responses of the connected user for the provided questions
     *
     * @param questionsId a comma separated ids of the questions that the user may responded
     * @return the ResponseEntity with status 200 (OK) and list of responses of the user
     * and the ResponseEntity with status 500 if the request body is invalid
     */
    @ApiOperation(notes = "Returns all the found responses of the connected user for the provided questions.",
            value = "Get all responses of the connected user for the questions",
            nickname = "getResponsesOfConnectUserForQuestions")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Question or user not found", response = RestErrorDto.class),
    })
    @Timed
    @GetMapping("/{questionsId}/responses/me")
    public List<UserResponseForQuestionDto> getResponsesOfConnectUserForQuestions(
            @ApiParam(value = "A comma separated ids of the questions that the user may responded example: 1, 2, 3",
                    required = true)
            @PathVariable("questionsId") String questionsId) {

        LOGGER.debug("REST request to get the responses of the connected user for the questions with ids {}", questionsId);
        return userResponseService.findResponsesOfUserForQuestions(USER_ID, commaDelimitedListToLongList(questionsId));
    }

    /**
     * POST  /responses/me : Save the responses of the connected user for the provided questions
     *
     * @param userResponseForQuestions questions ids and contents that the connected user entered
     * @return the ResponseEntity with status 200 (OK) and list of the saved responses of the user
     * and the ResponseEntity with status 500 if the request body is invalid
     */
    @ApiOperation(notes = "Add and update the responses of the connected user for the provided questions then " +
            "returns all the list of the saved responses of the user.",
            value = "Save the responses of the connected user for the provided questions",
            nickname = "getResponsesOfConnectUserForQuestions")
    @ApiResponses({
            @ApiResponse(code = 404, message = "Question or user not found", response = RestErrorDto.class),
    })
    @Timed
    @PostMapping("/responses/me")
    public List<UserResponseForQuestionDto> saveResponsesOfConnectUserForQuestions(@Valid @RequestBody
                                                                                           UserResponsesForQuestionListDto userResponseForQuestions) {
        LOGGER.debug("REST request to save the responses of the connected user for the questions {}", userResponseForQuestions);
        return userResponseService.saveResponsesOfUserForQuestions(USER_ID, userResponseForQuestions.getResponses());
    }

}