package com.hendisantika.survey.demosurvey.mapper;

import com.hendisantika.survey.demosurvey.domain.Question;
import com.hendisantika.survey.demosurvey.dto.QuestionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/18
 * Time: 09.04
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
@Service
public interface QuestionMapper {

    QuestionDto questionToQuestionDto(Question question);

    @Mapping(source = "questionId", target = "id")
    Question questionIdToQuestion(Long questionId);

    List<Question> questionsIdsListToQuestionList(List<Long> questionsIds);

}