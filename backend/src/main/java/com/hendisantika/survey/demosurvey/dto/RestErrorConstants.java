package com.hendisantika.survey.demosurvey.dto;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/05/18
 * Time: 09.37
 * To change this template use File | Settings | File Templates.
 */
public final class RestErrorConstants {

    public static final String ERR_INTERNAL_SERVER_ERROR = "error.internal";
    public static final String ERR_VALIDATION_ERROR = "error.validation";
    public static final String ERR_QUESTIONS_NOT_FOUND_ERROR = "error.questionNotFound";
    public static final String ERR_USERS_NOT_FOUND_ERROR = "error.userNotFound";

    private RestErrorConstants() {
    }
}
