package com.hendisantika.survey.demosurvey.service;

import com.hendisantika.survey.demosurvey.domain.Question;
import com.hendisantika.survey.demosurvey.dto.SubjectDto;
import com.hendisantika.survey.demosurvey.mapper.SubjectMapper;
import com.hendisantika.survey.demosurvey.repository.QuestionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/05/18
 * Time: 21.10
 * To change this template use File | Settings | File Templates.
 */
@Service
public class SimpleSubjectService implements SubjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSubjectService.class);

    private final QuestionDao questionDao;
    private final SubjectMapper subjectMapper;

    public SimpleSubjectService(QuestionDao questionDao, SubjectMapper subjectMapper) {
        this.questionDao = questionDao;
        this.subjectMapper = subjectMapper;
    }

    /**
     * find all the subjects and their questions
     *
     * @return a list of the questions with subjects
     */
    @Override
    public List<SubjectDto> findAllSubjectsAndQuestions() {
        LOGGER.debug("Request to get all the subjects and the questions");

        List<Question> questions = questionDao.findAllQuestionsAndSubjects();

        if (questions != null) {
            return subjectMapper.questionsToSubjectsDto(questions);
        }

        return Collections.emptyList();
    }

}
