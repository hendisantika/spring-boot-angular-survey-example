package com.hendisantika.survey.demosurvey.controller;

import com.codahale.metrics.annotation.Timed;
import com.hendisantika.survey.demosurvey.dto.RestFieldsErrorsDto;
import com.hendisantika.survey.demosurvey.dto.SubjectDto;
import com.hendisantika.survey.demosurvey.service.SubjectService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/05/18
 * Time: 22.51
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/api/v1/subjects")
public class SubjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectController.class);

    private final SubjectService subjectService;

    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    /**
     * GET  /subjects : get all the find all the subjects and their questions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list the subjects and their questions
     */
    @ApiOperation(notes = "Returns all the found subjects and their questions.",
            value = "Get all subjects and questions",
            nickname = "findAllSubjectsAndQuestions")
    @ApiResponses({
            /* We need to inject {@link RestFieldsErrorsDto} at least one so springfox can added it globally */
            @ApiResponse(code = 400, message = "Request content is invalid", response = RestFieldsErrorsDto.class)
    })
    @Timed
    @GetMapping
    public List<SubjectDto> findAllSubjectsAndQuestions() {
        LOGGER.debug("REST request to get all the subjects and the questions");

        return subjectService.findAllSubjectsAndQuestions();
    }

}