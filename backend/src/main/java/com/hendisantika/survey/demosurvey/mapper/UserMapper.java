package com.hendisantika.survey.demosurvey.mapper;

import com.hendisantika.survey.demosurvey.domain.User;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/18
 * Time: 18.15
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
@Service
public interface UserMapper {

    User toEntityFromId(Long id);

}