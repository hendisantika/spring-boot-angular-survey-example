package com.hendisantika.survey.demosurvey;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSurveyApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSurveyApplication.class, args);
    }
}
