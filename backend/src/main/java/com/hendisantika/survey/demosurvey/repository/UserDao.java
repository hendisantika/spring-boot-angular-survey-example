package com.hendisantika.survey.demosurvey.repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 09/05/18
 * Time: 22.29
 * To change this template use File | Settings | File Templates.
 */
public interface UserDao {
    /**
     * Returns the list of ids of the not found users using ids
     *
     * @param usersIds ids of the users to check
     * @return a list of the ids of the not found users
     * @throws DaoException             if there is an sql exception
     * @throws IllegalArgumentException if any given argument is invalid
     */
    List<Long> findNonExistingUsersByUsersIds(List<Long> usersIds);
}
