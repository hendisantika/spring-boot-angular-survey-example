package com.hendisantika.survey.demosurvey.mapper;

import com.hendisantika.survey.demosurvey.domain.Question;
import com.hendisantika.survey.demosurvey.domain.Subject;
import com.hendisantika.survey.demosurvey.dto.SubjectDto;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/18
 * Time: 09.05
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
@DecoratedWith(SubjectMapperDecorator.class)
@Service
public interface SubjectMapper {

    List<SubjectDto> questionsToSubjectsDto(List<Question> questions);

    SubjectDto toDto(Subject subject);

}
