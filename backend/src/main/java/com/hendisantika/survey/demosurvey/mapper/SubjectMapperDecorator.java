package com.hendisantika.survey.demosurvey.mapper;

import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.hendisantika.survey.demosurvey.domain.Question;
import com.hendisantika.survey.demosurvey.domain.Subject;
import com.hendisantika.survey.demosurvey.dto.QuestionDto;
import com.hendisantika.survey.demosurvey.dto.SubjectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/05/18
 * Time: 09.06
 * To change this template use File | Settings | File Templates.
 */
public abstract class SubjectMapperDecorator implements SubjectMapper {

    @Autowired
    @Qualifier("delegate")
    private SubjectMapper delegate;

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public List<SubjectDto> questionsToSubjectsDto(List<Question> questions) {
        Assert.notNull(questions, "Cannot map a null list of questions to a list of subject dtos");

        Multimap<Subject, Question> multimap = Multimaps.index(questions, Question::getSubject);

        return multimap.keySet().stream().map(subject -> {

            SubjectDto subjectDto = delegate.toDto(subject);

            List<QuestionDto> questionsDtos = multimap.get(subject).stream()
                    .map(question -> questionMapper.questionToQuestionDto(question)).collect(Collectors.toList());

            subjectDto.addQuestions(questionsDtos);
            return subjectDto;

        }).collect(Collectors.toList());
    }

}