package com.hendisantika.survey.demosurvey.config;

import com.google.common.collect.Sets;
import com.hendisantika.survey.demosurvey.dto.RestErrorDto;
import com.hendisantika.survey.demosurvey.dto.RestFieldsErrorsDto;
import io.swagger.annotations.ApiModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 12/05/18
 * Time: 08.36
 * To change this template use File | Settings | File Templates.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .globalOperationParameters(Collections.singletonList(acceptLanguageHeader()))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, Arrays.asList(badRequest(), internalServerError()))
                .globalResponseMessage(RequestMethod.POST, Arrays.asList(badRequest(), internalServerError()))
                .groupName("api")
                .produces(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .consumes(Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hendisantika.survey.demosurvey"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Survey API")
                .description("This is a survey api where you can see the list of subject and questions and respond to them")
                .contact(new Contact("Hendi Santika",
                        "https://gitlab.com/hendisantika/spring-boot-angular-survey-example",
                        ""))
                .license("Apache License Version 2.0")
                .licenseUrl("https://gitlab.com/hendisantika/spring-boot-angular-survey-example/blob/master/LICENSE")
                .version("1.0")
                .build();

    }

    private Parameter acceptLanguageHeader() {
        return new ParameterBuilder()
                .name("Accept-language")
                .description("Use the specified locale instead of English US")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();
    }


    private ResponseMessage internalServerError() {
        return responseMessage(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Something unexpected went wrong", RestErrorDto.class);
    }

    private ResponseMessage badRequest() {
        return responseMessage(HttpStatus.BAD_REQUEST.value(), "Request content is invalid", RestFieldsErrorsDto.class);
    }

    private ResponseMessage responseMessage(int status, String message, Class clazz) {
        return new ResponseMessageBuilder()
                .code(status)
                .message(message)
                .responseModel(new ModelRef(AnnotationUtils.findAnnotation(clazz, ApiModel.class).value()))
                .build();
    }

}