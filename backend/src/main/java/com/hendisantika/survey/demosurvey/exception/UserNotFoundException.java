package com.hendisantika.survey.demosurvey.exception;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/05/18
 * Time: 10.04
 * To change this template use File | Settings | File Templates.
 */
public class UserNotFoundException extends RuntimeException {

    private static final String ERROR_MESSAGE = "Users with ids %s was not found";

    private final List<Long> notFoundUsersIds;

    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param notFoundUsersIds the ids of the not found users
     */
    public UserNotFoundException(List<Long> notFoundUsersIds) {
        super(String.format(ERROR_MESSAGE, notFoundUsersIds));
        this.notFoundUsersIds = notFoundUsersIds;
    }

    /**
     * @return ids of the not found users
     */
    public List<Long> getNotFoundUsersIds() {
        return notFoundUsersIds;
    }
}
