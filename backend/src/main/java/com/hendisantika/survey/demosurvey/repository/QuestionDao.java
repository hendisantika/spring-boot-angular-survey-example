package com.hendisantika.survey.demosurvey.repository;

import com.hendisantika.survey.demosurvey.domain.Question;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/05/18
 * Time: 21.34
 * To change this template use File | Settings | File Templates.
 */
public interface QuestionDao {
    /**
     * find all questions with their subjects
     *
     * @return a list of the questions with subjects
     * @throws DaoException if there is an sql exception
     */
    List<Question> findAllQuestionsAndSubjects();

    /**
     * Returns the list of ids of the not found questions using ids
     *
     * @param questionsIds ids of the questions to check
     * @return a list of the ids of the not found questions
     * @throws DaoException             if there is an sql exception
     * @throws IllegalArgumentException if any given argument is invalid
     */
    List<Long> findNonExistingQuestionsByQuestionsIds(List<Long> questionsIds);
}
