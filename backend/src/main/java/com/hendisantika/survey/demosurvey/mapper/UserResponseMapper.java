package com.hendisantika.survey.demosurvey.mapper;

import com.hendisantika.survey.demosurvey.domain.User;
import com.hendisantika.survey.demosurvey.domain.UserResponse;
import com.hendisantika.survey.demosurvey.dto.UserResponseForQuestionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/05/18
 * Time: 18.27
 * To change this template use File | Settings | File Templates.
 */
@Mapper(componentModel = "spring")
@Service
public interface UserResponseMapper {

    @Mapping(source = "question.id", target = "questionId")
    UserResponseForQuestionDto userResponseToUserResponseForQuestionDto(UserResponse entity);

    List<UserResponseForQuestionDto> userResponseListToUserResponseForQuestionDtoList(List<UserResponse> entity);


    @Mapping(source = "userResponseForQuestion.questionId", target = "question.id")
    UserResponse userResponseForQuestionDtoToUserResponse(UserResponseForQuestionDto userResponseForQuestion);

    default List<UserResponse> userResponsesForQuestionsDtoToUserResponsesList(List<UserResponseForQuestionDto> userResponseForQuestions,
                                                                               User user) {

        return userResponseForQuestions.stream()
                .map(
                        userResponseForQuestionDto -> {
                            UserResponse userResponse = userResponseForQuestionDtoToUserResponse(userResponseForQuestionDto);
                            userResponse.setUser(user);
                            return userResponse;
                        })
                .collect(Collectors.toList());
    }

    default List<Long> userResponsesForQuestionsToQuestionsIdsList(List<UserResponseForQuestionDto> userResponsesForQuestions) {
        return userResponsesForQuestions.stream()
                .map(UserResponseForQuestionDto::getQuestionId)
                .collect(Collectors.toList());
    }

}
