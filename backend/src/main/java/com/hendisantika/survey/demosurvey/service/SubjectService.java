package com.hendisantika.survey.demosurvey.service;

import com.hendisantika.survey.demosurvey.dto.SubjectDto;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/05/18
 * Time: 21.11
 * To change this template use File | Settings | File Templates.
 */
public interface SubjectService {
    /**
     * find all the subjects and their questions
     *
     * @return a list of the questions with subjects
     */
    List<SubjectDto> findAllSubjectsAndQuestions();
}
