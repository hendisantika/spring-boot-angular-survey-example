package com.hendisantika.survey.demosurvey.repository;

import com.hendisantika.survey.demosurvey.domain.Question;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : backend
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/05/18
 * Time: 21.10
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class QuestionDaoTest {

    @Autowired
    private QuestionDao questionDao;

    @Test
    public void findAllSubjectsAndQuestions() {
        List<Question> questions = questionDao.findAllQuestionsAndSubjects();
        Assert.assertNotNull(questions);
        Assert.assertEquals(questions.size(), 4);
    }

}